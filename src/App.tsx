import React from 'react';
import { Row, Col, Button, Input } from 'antd';
import './App.css';


function App() {

const handleButtonClick = () => {
    console.log('Button clicked!');
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log('Input changed:', e.target.value);
  };

  return (
    <Row>
      <Col span={12}>
        <Button type="primary" onClick={handleButtonClick}>
          Click me
        </Button>
      </Col>
      <Col span={12}>
        <Input placeholder="Enter text" onChange={handleInputChange} />
      </Col>
    </Row>
  );
}

export default App;
